# download_map_tile

#### 介绍
地图切片下载器
QQ交流群：882188959

#### 软件架构
JDK1.8+SpringBoot+Javafx

#### 使用说明

![新建下载任务](https://images.gitee.com/uploads/images/2021/0430/140759_a4507015_2203130.png "屏幕截图.png")
![自定义范围下载](https://images.gitee.com/uploads/images/2021/0430/141258_7668b683_2203130.png "屏幕截图.png")
![下载任务进度条](https://images.gitee.com/uploads/images/2021/0430/140836_1926e1e9_2203130.png "屏幕截图.png")

#### 安装教程

使用native进行打包：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0430/141011_37dcb9f8_2203130.png "屏幕截图.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


微信交流：![微信二维码](https://images.gitee.com/uploads/images/2021/0430/142445_afcc9cdf_2203130.png "屏幕截图.png")
