package com.map.tile.utils;

import java.io.*;

public class FileUtils {


    public static final String FILE_PATH = "conf.json";
    /**
     * 读配置文件
     * @param
     * @return
     */
    public static String readFileContent() {
        File file = new File(FILE_PATH);
        isExists(file);
        BufferedReader reader = null;
        StringBuffer sbf = new StringBuffer();
        try {
            reader=new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
            String tempStr;
            while ((tempStr = reader.readLine()) != null) {
                sbf.append(tempStr);
            }
            reader.close();
            return sbf.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return sbf.toString();
    }

    /**
     * 写配置文件
     * @param
     * @return
     */
    public static void writeFileContent(String content) {
        File file = new File(FILE_PATH);
        isExists(file);
        try {
            FileOutputStream fos = new FileOutputStream(file,false);
            fos.write(content.getBytes());
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 判断文件是否存在
     * @param file
     * @return
     */
    private static void isExists(File file){
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static String readFileContent(String path) {
        File file = new File(path);
        isExists(file);
        BufferedReader reader = null;
        StringBuffer sbf = new StringBuffer();
        try {
            reader=new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
            String tempStr;
            while ((tempStr = reader.readLine()) != null) {
                sbf.append(tempStr);
            }
            reader.close();
            return sbf.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return sbf.toString();
    }

    public static void main(String[] args) {
        //String shp = readFileContent("E:\\model\\金华\\SHP\\菜地.shp");
        String shp = readFileContent("E:\\model\\Tile_+032_+000.b3dm");
        System.out.println(shp+">>>\n");
        String shp2 = BinstrToStr(shp);
        System.out.println(shp2);
    }

    // 将二进制字符串转换成Unicode字符串
    private static String BinstrToStr(String binStr) {
        String[] tempStr = StrToStrArray(binStr);
        char[] tempChar = new char[tempStr.length];
        for (int i = 0; i < tempStr.length; i++) {
            tempChar[i] = BinstrToChar(tempStr[i]);
        }
        return String.valueOf(tempChar);
    }

    // 将初始二进制字符串转换成字符串数组，以空格相隔
    private static String[] StrToStrArray(String str) {
        return str.split(" ");
    }

    // 将二进制字符串转换为char
    private static char BinstrToChar(String binStr) {
        int[] temp = BinstrToIntArray(binStr);
        int sum = 0;
        for (int i = 0; i < temp.length; i++) {
            sum += temp[temp.length - 1 - i] << i;
        }
        return (char) sum;
    }

    // 将二进制字符串转换成int数组
    private static int[] BinstrToIntArray(String binStr) {
        char[] temp = binStr.toCharArray();
        int[] result = new int[temp.length];
        for (int i = 0; i < temp.length; i++) {
            result[i] = temp[i] - 48;
        }
        return result;
    }

    // uint32
    public static long bytes2int(byte[] src){
        int firstByte = 0;
        int secondByte = 0;
        int thirdByte = 0;
        int fourthByte = 0;
        int index = 0;
        long anUnsignedInt = 0;
        firstByte = (0x000000FF & ((int) src[index]));
        secondByte = (0x000000FF & ((int) src[index+1]));
        thirdByte = (0x000000FF & ((int) src[index+2]));
        fourthByte = (0x000000FF & ((int) src[index+3]));
        anUnsignedInt = ((firstByte<<24|secondByte<<16|thirdByte<<8|fourthByte)) & 0x00000000FFFFFFFF;
        return anUnsignedInt ;

    }

}
