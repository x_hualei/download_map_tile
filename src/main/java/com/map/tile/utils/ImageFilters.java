/*
package com.map.tile.utils;

import com.map.tile.controller.MainController;
import com.map.tile.controller.MapController;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class ImageFilters {

* - 反色处理 -

    public static Mat inverse(Mat image) {
        int width = image.cols();
        int height = image.rows();
        int dims = image.channels();
        byte[] data = new byte[width*height*dims];
        image.get(0, 0, data);

        int index = 0;
        int r=0, g=0, b=0;
        for(int row=0; row<height; row++) {
            for(int col=0; col<width*dims; col+=dims) {
                index = row*width*dims + col;
                b = data[index]&0xff;
                g = data[index+1]&0xff;
                r = data[index+2]&0xff;

                r = 255 - r;
                g = 255 - g;
                b = 255 - b;

                data[index] = (byte)b;
                data[index+1] = (byte)g;
                data[index+2] = (byte)r;
            }
        }

        image.put(0, 0, data);
        return image;
    }

    // 亮度提升
    public static Mat brightness(Mat image) {
        Mat dst = new Mat();
        Mat black = Mat.zeros(image.size(), image.type());
        Core.addWeighted(image, 1.2, black, 0.5, 0, dst);
        return dst;
    }

    // 亮度降低
    public static Mat darkness(Mat image) {
        Mat dst = new Mat();
        Mat black = Mat.zeros(image.size(), image.type());
        Core.addWeighted(image, 0.5, black, 0.5, 0, dst);
        return dst;
    }

    // 灰度
    public static Mat gray(Mat image) {
        Mat gray = new Mat();
        Imgproc.cvtColor(image, gray, Imgproc.COLOR_BGR2GRAY);
        return gray;
    }

    // 锐化
    public static Mat sharpen(Mat image) {
        Mat dst = new Mat();
        float[] sharper = new float[]{0, -1, 0, -1, 5, -1, 0, -1, 0};
        Mat operator = new Mat(3, 3, CvType.CV_32FC1);
        operator.put(0, 0, sharper);
        Imgproc.filter2D(image, dst, -1, operator);
        return dst;
    }

    // 高斯模糊
    public static Mat blur(Mat image) {
        Mat dst = new Mat();
        Imgproc.GaussianBlur(image, dst, new Size(15, 15), 0);
        return dst;
    }

    // 梯度
    public static Mat gradient(Mat image) {
        Mat grad_x = new Mat();
        Mat grad_y = new Mat();
        Mat abs_grad_x = new Mat();
        Mat abs_grad_y = new Mat();

        Imgproc.Sobel(image, grad_x, CvType.CV_32F, 1, 0);
        Imgproc.Sobel(image, grad_y, CvType.CV_32F, 0, 1);
        Core.convertScaleAbs(grad_x, abs_grad_x);
        Core.convertScaleAbs(grad_y, abs_grad_y);
        grad_x.release();
        grad_y.release();
        Mat gradxy = new Mat();
        Core.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 10, gradxy);
        return gradxy;
    }

    */
/**
     * 读取网络图片
     * @param urlStr
     * @return
     *//*

    public static final BufferedImage bufferReadUrl(String urlStr){
        BufferedImage image = null;
        try
        {
            URL url = new URL(urlStr);
            image = ImageIO.read(url);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return image;
    }

    */
/**
     * 图片文件保存
     * @param fileName
     * @param mat
     *//*

    public static final void matSave(String fileName,Mat mat) {
        BufferedImage buff = matToBuffer(".png",mat);
        bufferSave(fileName,buff);
    }

    */
/**
     * 图片文件保存
     * @param fileName
     * @param buff
     *//*

    public static final void bufferSave(String fileName,BufferedImage buff) {
        try {
            ImageIO.write(buff, "PNG", new File(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static final Mat matResize(Mat mat, int width, int height){
        Mat target = new Mat(width,height,mat.type());
        Imgproc.resize(mat, target, new Size(width,height), 0, 0, Imgproc.INTER_LINEAR);
        return target;
    }

    */
/**
     * Mat转换为BufferedImage，已测，可用
     * @param fileExt
     * @param mat
     * @return
     *//*

    public static final BufferedImage matToBuffer(String fileExt,Mat mat)
    {
        MatOfByte mob = new MatOfByte();
        Imgcodecs.imencode(fileExt, mat, mob);
        byte[] byteArray = mob.toArray();
        BufferedImage bufImage = null;
        try {
            InputStream in = new ByteArrayInputStream(byteArray);
            bufImage = ImageIO.read(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bufImage;
    }

    */
/**
     * BufferedImage转换为Mat，已测，可用
     * @param bufferedImage
     * @param imgType
     * @return
     *//*

    public static Mat bufferToMat(BufferedImage bufferedImage, int imgType)
    {
        final int matType = CvType.CV_8UC3;

        if (bufferedImage == null) {
            throw new IllegalArgumentException("bufferToMat-> BufferedImage == null");
        }

        if (bufferedImage.getType() != imgType) {
            BufferedImage image = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), imgType);
            Graphics2D g = image.createGraphics();
            try {
                g.setComposite(AlphaComposite.Src);
                g.drawImage(bufferedImage, 0, 0, null);
            } finally {
                g.dispose();
            }
        }

        byte[] pixels = ((DataBufferByte) bufferedImage.getRaster().getDataBuffer()).getData();
        Mat mat = Mat.eye(bufferedImage.getHeight(), bufferedImage.getWidth(), matType);
        mat.put(0, 0, pixels);

        return mat;
    }

    public static byte[] getByte(Mat mat) {
        int width = mat.cols();
        int height = mat.rows();
        int dims = mat.channels();
        byte[] rgbdata = new byte[width*height*dims];
        mat.get(0, 0, rgbdata);
        return rgbdata;
    }

    public static void outRgb(Mat mat) {
        int width = mat.cols();
        int height = mat.rows();
        int dims = mat.channels();
        byte[] data = new byte[width*height*dims];
        mat.get(0, 0, data);
        int index = 0;
        int r=0, g=0, b=0;
        for(int row=0; row<height; row++) {
            for(int col=0; col<width*dims; col+=dims) {
                index = row*width*dims + col;
                b = data[index]&0xff;
                g = data[index+1]&0xff;
                r = data[index+2]&0xff;
                System.out.println(row+","+col/3+":rgb("+r+","+g+","+b+")");
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Mat mat1 = MapController.bufferToMat(MapController.bufferReadUrl("http://192.168.0.240:10099/tianditu/1/0/0.png"),MapController.bufferReadUrl("http://192.168.0.240:10099/tianditu/1/0/0.png").getType());
        Mat mat = inverse(mat1);
        BufferedImage bufferedImage = MapController.matToBuffer(".png",mat);
        MapController.bufferSave("E:\\model\\map\\mat.png",bufferedImage);
    }

}
*/
