package com.map.tile.controller;

import de.felixroske.jfxsupport.FXMLController;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import netscape.javascript.JSObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;


@FXMLController
public class MapController {

    @FXML
    private WebView webView;
    @Autowired
    private MainController mainController;

    private JavaConnector javaConnector = new JavaConnector();

    @FXML
    private void initialize(){
        String url = MapController.class.getResource("/html/hello.html").toExternalForm();
        webView.getEngine().load(url);
        webView.getEngine().getLoadWorker().stateProperty().addListener((ObservableValue<? extends Worker.State> ov, Worker.State oldState, Worker.State newState) -> {
            if (newState == Worker.State.SUCCEEDED) {
                JSObject win = (JSObject) webView.getEngine().executeScript("window");
                win.setMember("javaConnector", javaConnector);// 设置变量
            }
        });
    }

    public static void main(String[] args) throws IOException {

    }

    public class JavaConnector {
        public void close(String value) {
            System.out.println(value);
            String[] strArr = value.split(",");
            mainController.setRange(strArr[6]+","+strArr[7]+","+strArr[2]+","+strArr[3]);
            Stage stage = (Stage) webView.getScene().getWindow();
            stage.close();
        }
    }

}
