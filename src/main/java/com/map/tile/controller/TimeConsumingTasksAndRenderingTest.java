package com.map.tile.controller;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;

import java.util.Map;

/**
 * 异步耗时任务操作和渲染测试
 * @author eguid
 *
 */
public class TimeConsumingTasksAndRenderingTest extends Application {

	Integer width,height;

	Scene scene;

	GraphicsContext gc;
	/**
	 * 获取输出参数
	 * @param key
	 * @return
	 */
	private String getParam(String key) {
		Map<String, String> params=getParameters().getNamed();
		return params.get(key);
	}

	public TimeConsumingTasksAndRenderingTest() {
		//与init，start，stop不属于同一个线程
		System.out.println(("创建"+this.getClass().getSimpleName()+"对象，顺序：2，当前线程： "+Thread.currentThread().getName()));
	}

	@Override
	public void init() throws Exception {
		System.out.println("初始化"+this.getClass().getSimpleName()+",对象，顺序：3，当前线程： "+Thread.currentThread().getName());
		String widthS=getParam("width");
		if(widthS!=null&&widthS.length()<5) {
			width=Integer.valueOf(widthS);
		}

		String heightS=getParam("height");
		if(heightS!=null&&heightS.length()<5) {
			height=Integer.valueOf(heightS);
		}

		//创建画布
		BorderPane root=new BorderPane();
		Canvas canvas=new Canvas(width, height);
		gc = canvas.getGraphicsContext2D();

		root.getChildren().add(canvas);
		scene=new Scene(root,width, height);

		super.init();
	}


	@Override
	public void stop() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("销毁"+this.getClass().getSimpleName()+"对象，顺序：5，当前线程： "+Thread.currentThread().getName());
		super.stop();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		System.out.println(this.getClass().getSimpleName()+"开始显示窗口，顺序：4，当前线程："+Thread.currentThread().getName());

//		primaryStage.setFocused(true);
		primaryStage.setAlwaysOnTop(true);
		primaryStage.setTitle("canvas测试窗口");
//		primaryStage.setOpacity(50);
		primaryStage.setResizable(false);

		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		primaryStage.show();


		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					//模拟耗时任务
					Thread.sleep(5000);
				} catch (InterruptedException e) {

				}
				//耗时任务执行完，一般需要将结果渲染到javafx窗口中，就需要使用Platform.runLater(task)操作把渲染操作放进去执行
				/**在JavaFX Application Thread线程中执行一些阻塞操作，
				 * javafx将每个任务都放进了JavaFX Application Thread的一个待执行队列中，
				 * JavaFX Application Thread会逐个阻塞的执行这些任务
				 */
				//JavaFX Application Thread会逐个阻塞的执行这些任务
				Platform.runLater(new Task<Integer>() {
					@Override
					protected Integer call() {
						gc.setFill(Color.BLUE);
						gc.fillText("测试eguid", 200,200);//绘制文字

						gc.setFill(Color.RED);
						gc.fillRect(75,75,100,100);//绘制矩形

						gc.setFill(Color.GREEN);
						gc.fillRoundRect(50,300, 200, 200, 100, 50);//绘制圆角矩形

						gc.setFill(Color.YELLOWGREEN);
						//坐标x,坐标y，宽，高，起始度数，重点度数，闭合类型（ROUND：圆形，CHORD：弦，open:打开）
						gc.fillArc(300, 100, 50, 50, 60, 270, ArcType.OPEN);//绘制不完整的圆形
						gc.fillArc(300, 150, 50, 50, 60, 270, ArcType.CHORD);//绘制不完整的圆形
						gc.fillArc(300, 200, 50, 50, 60, 270, ArcType.ROUND);//绘制不完整的圆形
						gc.fillArc(300, 250, 50, 50, 0, 360, ArcType.ROUND);//绘制完整的原型

						return 1;
					}
				});
			}
		}).start();
	}

//打开窗口
	public static void main(String[] args) {
		launch(new String[]{"--width=800","--height=600"});
	}
}
