package com.map.tile.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.map.tile.XxApplication;
import com.map.tile.utils.FileUtils;
import com.map.tile.views.MapView;
import com.map.tile.views.MassageView;
import de.felixroske.jfxsupport.FXMLController;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@FXMLController
public class MainController {

    @FXML
    private ComboBox comboBoxLevelSmall;
    @FXML
    private ComboBox comboBoxLevelBig;
    @FXML
    private ComboBox comboBoxMap;
    @FXML
    private TextField range;
    @FXML
    private TabPane tabPane;
    public void setRange(String range) {
        this.range.setText(range);
    }
    @FXML
    private TextArea textArea;
    @FXML
    private Pane progressPane;
    @FXML
    private Pane progressPane2;
    @FXML
    private Pane progressPane3;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private ProgressBar progressBar2;
    @FXML
    private ProgressBar progressBar3;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private ProgressIndicator progressIndicator2;
    @FXML
    private ProgressIndicator progressIndicator3;
    @FXML
    private Pane suspend;
    @FXML
    private Pane suspend2;
    @FXML
    private Pane suspend3;
    @FXML
    private Label title;
    @FXML
    private Label title2;
    @FXML
    private Label title3;
    @FXML
    private Label state;
    @FXML
    private Label state2;
    @FXML
    private Label state3;
    @FXML
    private Label creatTime;
    @FXML
    private Label creatTime2;
    @FXML
    private Label creatTime3;
    @FXML
    private Label levelLabel;
    @FXML
    private Label levelLabel2;
    @FXML
    private Label levelLabel3;
    @FXML
    private Label mag;

    @FXML
    private TextField filePath;
    @FXML
    private TextArea token;

    @Autowired
    private MapView mapView;
    @Autowired
    private MassageView massageView;
    @Autowired
    private MassageController massageController;

    private static JSONArray jsonArray = new JSONArray();

    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36 Edg/88.0.705.68";

    /**
     * 初始化界面
     */
    @FXML
    private void initialize(){
        List<Integer> levelList = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22);
        ObservableList<Integer> levelListob = FXCollections.observableArrayList(levelList);
        comboBoxLevelSmall.setItems(levelListob);
        comboBoxLevelSmall.getSelectionModel().select(0);
        comboBoxLevelBig.setItems(levelListob);
        comboBoxLevelBig.getSelectionModel().select(5);
        List<String> mapList = Arrays.asList("天地图影像","谷歌地图影像");
        ObservableList<String> mapListob = FXCollections.observableArrayList(mapList);
        comboBoxMap.setItems(mapListob);
        comboBoxMap.getSelectionModel().select(1);
        //range.setText("70,50,130,10");
        range.setText("117.865104,31.685124,122.347625,27.054879");//浙江省范围
        textArea.setText("=================================================系统日志=================================================\n");
        filePath.setText("E:\\model\\google");
        token.setText("tk=******************");
        String fileContent = FileUtils.readFileContent();
        if(!fileContent.equals("")){
            jsonArray = JSONArray.parseArray(fileContent);
            for (int i=0;i<jsonArray.size();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                double completion = jsonArray.getJSONObject(i).getLong("count").doubleValue() / jsonArray.getJSONObject(i).getLong("countAll").doubleValue();
                if(i == 0){
                    progressPane.setVisible(true);
                    title.setText("任务"+i+"--("+range.getText()+")级别："+jsonObject.getString("comboBoxLevelSmall")+"-"+jsonObject.getString("comboBoxLevelBig"));
                    creatTime.setText(jsonObject.getString("creationTime"));
                    progressBar.setProgress(completion);
                    progressIndicator.setProgress(completion);
                }else if(i == 1){
                    progressPane2.setVisible(true);
                    title2.setText("任务"+i+"--("+range.getText()+")级别："+jsonObject.getString("comboBoxLevelSmall")+"-"+jsonObject.getString("comboBoxLevelBig"));
                    creatTime2.setText(jsonObject.getString("creationTime"));
                    progressBar2.setProgress(completion);
                    progressIndicator2.setProgress(completion);
                }else if(i == 2){
                    progressPane3.setVisible(true);
                    title3.setText("任务"+i+"--("+range.getText()+")级别："+jsonObject.getString("comboBoxLevelSmall")+"-"+jsonObject.getString("comboBoxLevelBig"));
                    creatTime3.setText(jsonObject.getString("creationTime"));
                    progressBar3.setProgress(completion);
                    progressIndicator3.setProgress(completion);
                }
            }
        }
    }

    @FXML
    private void globalClick(final Event event) {
        range.setText("-180,180,180,0");
    }

    @FXML
    private void chinaClick(final Event event) {
        range.setText("70,54,133,2");
    }

    @FXML
    private void customClick(final Event event) {
        XxApplication.showView(mapView.getClass(), Modality.NONE);
    }

    @FXML
    private void folderClick(final Event event) {
        DirectoryChooser file=new DirectoryChooser();
        file.setTitle("选择输出文件夹");
        Stage stage = (Stage) range.getScene().getWindow();
        File newFolder = file.showDialog(stage);
        filePath.setStyle("-fx-text-fill: -tab-text-color;-fx-text-base-color: black;");
        if(newFolder!=null){
            filePath.setText(newFolder.getPath());
        }
    }

    @FXML
    private void suspend(final Event event) throws URISyntaxException, MalformedURLException {
        if(jsonArray.getJSONObject(0).getBoolean("isSuspend")){
            state.setText("下载请求中...");
            state.setStyle("-fx-text-fill: -tab-text-color;-fx-text-base-color: #89ffb6;");
            suspend.setStyle("-fx-background-image: url("+this.getClass().getResource("/example/img/suspend2.png").toURI().toURL()+"); -fx-background-size: 24px 24px;-fx-cursor: hand;");
            jsonArray.getJSONObject(0).put("start", true);
            jsonArray.getJSONObject(0).put("isSuspend", false);
        }else {
            state.setText("暂停中...");
            state.setStyle("-fx-text-fill: -tab-text-color;-fx-text-base-color: red;");
            suspend.setStyle("-fx-background-image: url("+this.getClass().getResource("/example/img/start2.png").toURI().toURL()+"); -fx-background-size: 24px 24px;-fx-cursor: hand;");
            jsonArray.getJSONObject(0).put("isSuspend", true);
        }
    }

    @FXML
    private void suspend2(final Event event) throws URISyntaxException, MalformedURLException {
        if(jsonArray.getJSONObject(1).getBoolean("isSuspend")){
            state2.setText("下载请求中...");
            state2.setStyle("-fx-text-fill: -tab-text-color;-fx-text-base-color: #89ffb6;");
            suspend2.setStyle("-fx-background-image: url("+this.getClass().getResource("/example/img/suspend2.png").toURI().toURL()+"); -fx-background-size: 24px 24px;-fx-cursor: hand;");
            jsonArray.getJSONObject(1).put("start", true);
            jsonArray.getJSONObject(1).put("isSuspend", false);
        }else {
            state2.setText("暂停中...");
            state2.setStyle("-fx-text-fill: -tab-text-color;-fx-text-base-color: red;");
            suspend2.setStyle("-fx-background-image: url("+this.getClass().getResource("/example/img/start2.png").toURI().toURL()+"); -fx-background-size: 24px 24px;-fx-cursor: hand;");
            jsonArray.getJSONObject(1).put("isSuspend", true);
        }
    }

    @FXML
    private void suspend3(final Event event) throws URISyntaxException, MalformedURLException {
        if(jsonArray.getJSONObject(2).getBoolean("isSuspend")){
            state3.setText("下载请求中...");
            state3.setStyle("-fx-text-fill: -tab-text-color;-fx-text-base-color: #89ffb6;");
            suspend3.setStyle("-fx-background-image: url("+this.getClass().getResource("/example/img/suspend2.png").toURI().toURL()+"); -fx-background-size: 24px 24px;-fx-cursor: hand;");
            jsonArray.getJSONObject(2).put("start", true);
            jsonArray.getJSONObject(2).put("isSuspend", false);
        }else {
            state3.setText("暂停中...");
            state3.setStyle("-fx-text-fill: -tab-text-color;-fx-text-base-color: red;");
            suspend3.setStyle("-fx-background-image: url("+this.getClass().getResource("/example/img/start2.png").toURI().toURL()+"); -fx-background-size: 24px 24px;-fx-cursor: hand;");
            jsonArray.getJSONObject(2).put("isSuspend", true);
        }
    }

    @FXML
    private void cancel(final Event event) {
        state.setText("任务已取消...");
        state.setStyle("-fx-text-fill: -tab-text-color;-fx-text-base-color: red;");
        jsonArray.remove(0);
        FileUtils.writeFileContent(jsonArray.toJSONString());//写入文件
    }

    @FXML
    private void cancel2(final Event event) {
        state2.setText("任务已取消...");
        state2.setStyle("-fx-text-fill: -tab-text-color;-fx-text-base-color: red;");
        jsonArray.remove(1);
        FileUtils.writeFileContent(jsonArray.toJSONString());//写入文件
    }

    @FXML
    private void cancel3(final Event event) {
        state3.setText("任务已取消...");
        state3.setStyle("-fx-text-fill: -tab-text-color;-fx-text-base-color: red;");
        jsonArray.remove(2);
        FileUtils.writeFileContent(jsonArray.toJSONString());//写入文件
    }

    @FXML
    private void open(final Event event) {
        //Desktop.getDesktop().open(new File("E:\\model\\map"));
        try {
            Desktop.getDesktop().open(new File(jsonArray.getJSONObject(0).getString("filePath")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void open2(final Event event) {
        try {
            Desktop.getDesktop().open(new File(jsonArray.getJSONObject(1).getString("filePath")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void open3(final Event event) {
        try {
            Desktop.getDesktop().open(new File(jsonArray.getJSONObject(2).getString("filePath")));
        } catch (IOException e) {
            e.printStackTrace();
        };
    }

    /**
     * 添加下载任务
     * @param event
     * @throws IOException
     */
    @FXML
    private void submitController(final Event event) throws IOException {
        int levelBig = Integer.parseInt(comboBoxLevelBig.getSelectionModel().getSelectedItem().toString());
        int levelSmall = Integer.parseInt(comboBoxLevelSmall.getSelectionModel().getSelectedItem().toString());
        String comboBoxMapStr = comboBoxMap.getSelectionModel().getSelectedItem().toString();
        String rangeStr = range.getText();
        String filePathStr = filePath.getText();
        if(filePath.getText().equals("") || filePath.getText().equals("请选择输出文件夹或直接输入下载路径")){
            filePath.setStyle("-fx-text-fill: -tab-text-color;-fx-text-base-color: red;");
            filePath.setText("请选择输出文件夹");
        }else if(jsonArray.size()>2){
            filePath.setStyle("-fx-text-fill: -tab-text-color;-fx-text-base-color: red;");
            filePath.setText("最多只能创建三个任务");
        }else if(jsonArray.stream().filter(m-> comboBoxMapStr.equals(JSONObject.parseObject(m.toString()).getString("comboBoxMap"))
                && rangeStr.equals(JSONObject.parseObject(m.toString()).getString("range"))
                && levelBig==JSONObject.parseObject(m.toString()).getInteger("comboBoxLevelBig")
                && levelSmall==JSONObject.parseObject(m.toString()).getInteger("comboBoxLevelSmall")
                && filePathStr.equals(JSONObject.parseObject(m.toString()).getString("filePath"))).collect(Collectors.toList()).size()>0) {
            mag.setText("重复的任务");
        }else {
            int z=levelSmall;
            long countAll = 0;
            while (z <= levelBig) {
                int[] tileArr = tileArr(range.getText(), z);
                countAll += (long) ((tileArr[2] - tileArr[0])+1) * (long) ((tileArr[3] - tileArr[1])+1);
                z++;
            }
            int code = jsonArray.size();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code",code);//既是任务名称也是该任务所在的数组下标
            jsonObject.put("comboBoxMap",comboBoxMapStr);//地图类型
            jsonObject.put("comboBoxLevelSmall",levelSmall);//最小级别
            jsonObject.put("comboBoxLevelBig",levelBig);//最大级别
            jsonObject.put("range",rangeStr);//范围
            jsonObject.put("filePath",filePathStr);//存放路径
            jsonObject.put("start",true);//开始状态
            jsonObject.put("success",false);//成功状态
            jsonObject.put("completion",0);//完成百分比
            jsonObject.put("isSuspend",false);//是否暂停
            jsonObject.put("creationTime", LocalDateTime.now());//创建时间
            jsonObject.put("state", "");//存放暂停时的状态  手动暂停或者超过天地图日调用次数--断点续传
            jsonObject.put("countAll", countAll);//总切片数
            jsonObject.put("token", token.getText());//总切片数
            jsonArray.add(jsonObject);
            FileUtils.writeFileContent(jsonArray.toJSONString());//写入文件
            if(code == 0){
                progressPane.setVisible(true);
                title.setText("任务"+code+"--("+range.getText()+")级别："+jsonObject.getString("comboBoxLevelSmall")+"-"+jsonObject.getString("comboBoxLevelBig"));
                creatTime.setText(jsonObject.getString("creationTime"));
            }else if(code == 1){
                progressPane2.setVisible(true);
                title2.setText("任务"+code+"--("+range.getText()+")级别："+jsonObject.getString("comboBoxLevelSmall")+"-"+jsonObject.getString("comboBoxLevelBig"));
                creatTime2.setText(jsonObject.getString("creationTime"));
            }else if(code == 2){
                progressPane3.setVisible(true);
                title3.setText("任务"+code+"--("+range.getText()+")级别："+jsonObject.getString("comboBoxLevelSmall")+"-"+jsonObject.getString("comboBoxLevelBig"));
                creatTime3.setText(jsonObject.getString("creationTime"));
            }
            tabPane.getSelectionModel().select(1);
        }
    }

    /**
     * 监听数据变化
     * @throws IOException
     */
    @Scheduled(fixedRate = 1000)
    public void cron() throws IOException {
        for (int i=0;i<jsonArray.size();i++){
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            if(jsonObject.getBoolean("start") && !jsonObject.getBoolean("success") && !jsonObject.getBoolean("isSuspend")){
                jsonObject.put("startTime",System.currentTimeMillis()); //记录开始时间
                jsonObject.put("start",false); //开始状态设置为已开始
                int levelBig = jsonObject.getInteger("comboBoxLevelBig");
                int levelSmall = jsonObject.getInteger("comboBoxLevelSmall");
                int z;
                if(jsonObject.getString("state").equals("")){
                    z = levelSmall;
                }else {
                    String[] arr = jsonObject.getString("state").split(",");
                    z = Integer.parseInt(arr[1]);
                }
                while (z <= levelBig) {
                    try {
                        int[] tileArr = tileArr(jsonObject.getString("range"),z);
                        String[] arr = jsonObject.getString("state").split(",");
                        if(!jsonArray.getJSONObject(i).getString("state").equals("") && z == Integer.parseInt(arr[1])){
                            tileArr = new int[]{Integer.parseInt(arr[0]),tileArr[1],tileArr[2],tileArr[3]};
                        }
                        level(jsonObject.getInteger("code"),jsonObject.getString("comboBoxMap"),z,jsonObject.getString("filePath"),tileArr,jsonObject.getString("token"));
                    } catch (IOException | InterruptedException e) {
                        e.printStackTrace();
                    }
                    int finalZ = z;
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            if(!jsonObject.getBoolean("isSuspend")){
                                textArea.appendText("任务"+jsonObject.getString("code")+"======================第"+ finalZ +"级别数据下载完成...\n");
                            }
                        }
                    });
                    z++;
                }
                if(!jsonArray.getJSONObject(i).getBoolean("isSuspend")) {
                    jsonObject.put("success",true); //成功状态设置为已成功
                    long time = System.currentTimeMillis() - jsonObject.getLong("startTime");
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            textArea.appendText("=========================================任务" + jsonObject.getString("code") + "数据下载完成>>>耗时："+time/1000+"s=========================================\n");
                        }
                    });
                }
            }
        }
//        if(LocalDate.now().isAfter(LocalDate.parse("2021-08-02"))){
//            System.exit(0);
//        }
    }

    ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());//定长线程池

    /**
     * 按层级下载
     * @param code
     * @param type
     * @param zoom
     * @param filePath
     * @param tileArr
     * @param tk
     * @throws IOException
     * @throws InterruptedException
     */
    public void level(int code,String type,Integer zoom,String filePath,int[] tileArr,String tk) throws IOException, InterruptedException {
        //System.out.println(tileArr[0]+"--"+tileArr[1]+"--"+tileArr[2]+"--"+tileArr[3]);
        long countLevel = (long) (tileArr[2] - tileArr[0]) * (long) (tileArr[3] - tileArr[1]);
        //ExecutorService threadPool = Executors.newCachedThreadPool();//可缓存线程池
        Boolean b = true;//存最小的i值
        for(int i=tileArr[0];i<=tileArr[2];i++) {
            if (!jsonArray.getJSONObject(code).getBoolean("isSuspend")) {
                CountDownLatch countDownLatch = new CountDownLatch(tileArr[3] - tileArr[1]);
                for (int j = tileArr[1]; j <= tileArr[3]; j++) {
                    long finalCount = countLevel--;
                    int finalI = i;
                    int finalJ = j;
                    threadPool.execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                download(code, type, finalI, finalJ, zoom, filePath,tk);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            countDownLatch.countDown();
                        }
                    });
                }
                countDownLatch.await();
                int finalI = i;
                int finalCount = tileArr[2] - i;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (!jsonArray.getJSONObject(code).getBoolean("isSuspend")) {
                            String state = finalI + "," + zoom;
                            long count = jsonArray.getJSONObject(code).getLong("countAll") - getRemain(state,jsonArray.getJSONObject(code).getString("range"),jsonArray.getJSONObject(code).getInteger("comboBoxLevelSmall"),jsonArray.getJSONObject(code).getInteger("comboBoxLevelBig"));
                            double completion = count / jsonArray.getJSONObject(code).getDouble("countAll");
                            if (code == 0) {
                                progressBar.setProgress(completion);
                                progressIndicator.setProgress(completion);
                            } else if (code == 1) {
                                progressBar2.setProgress(completion);
                                progressIndicator2.setProgress(completion);
                            } else if (code == 2) {
                                progressBar3.setProgress(completion);
                                progressIndicator3.setProgress(completion);
                            }
                            if (textArea.getText().length() > 10000) {
                                textArea.setText("");
                            }
                            jsonArray.getJSONObject(code).put("state", state);//记录层、级
                            jsonArray.getJSONObject(code).put("count", count);//记录剩余切片数量
                            FileUtils.writeFileContent(jsonArray.toJSONString());//写入文件
                            textArea.appendText("已完成任务" + code + "第" + zoom + "级别下" + finalI + "层数据下载-该级别剩余层数:" + finalCount + ",剩余切片总数：" + count + "/" + jsonArray.getJSONObject(code).getLong("countAll") + "\n");
                        }
                    }
                });
            }
        }
    }

    /**
     * 计算剩余切片数量
     * @param state
     * @param range
     * @param comboBoxLevelSmall
     * @param comboBoxLevelBig
     * @return
     */
    public static long getRemain(String state,String range,int comboBoxLevelSmall,int comboBoxLevelBig){
        String[] stateArr = state.split(",");
        long countAll = 0;
        int z = Integer.parseInt(stateArr[1]);
        int zoom = Integer.parseInt(stateArr[1]);
        while (z <= comboBoxLevelBig){
            int[] tileArr = tileArr(range, z);
            if(z == zoom){
                countAll += (long) ((tileArr[2] - Long.parseLong(stateArr[0]))) * (long) ((tileArr[3] - tileArr[1]) + 1);
            }else {
                countAll += (long) ((tileArr[2] - tileArr[0])+1) * (long) ((tileArr[3] - tileArr[1])+1);
            }
            z++;
        }
        return countAll;
    }

    /**
     * 根据范围和层级计算瓦片起始索引和结束索引
     * @param range
     * @param zoom
     * @return
     */
    public static int[] tileArr(String range,int zoom){
        String[] rangeArr = range.split(",");
        Integer[] tileArr = deg2num(Double.parseDouble(rangeArr[0]),Double.parseDouble(rangeArr[1]),zoom);
        Integer[] tileArr2 = deg2num(Double.parseDouble(rangeArr[2]),Double.parseDouble(rangeArr[3]),zoom);
        return new int[]{tileArr[0],tileArr[1],tileArr2[0],tileArr2[1]};
    }

    /**
     * 根据经纬度和层级计算瓦片索引
     * @param lon_deg
     * @param lat_deg
     * @param zoom
     * @return
     */
    public static Integer[] deg2num(Double lon_deg,Double lat_deg,int zoom){
        Double lat_rad = Math.toRadians(lat_deg);
        int n = 2 << (zoom-1);
        int xTile = (int) ((lon_deg + 180.0) / 360.0 * n);
        int yTile = (int) ((1.0 - Math.log(Math.tan(lat_rad) + (1 / Math.cos(lat_rad)))/ Math.PI) / 2.0 * n);
        return new Integer[]{xTile,yTile};
    }

    /**
     * 切片下载
     * @param code
     * @param type
     * @param i
     * @param j
     * @param z
     * @param filePath
     * @param tk
     * @throws IOException
     */
    public void download(int code,String type, Integer i,Integer j,Integer z,String filePath,String tk) throws IOException {
        String uri = "";
        if(type.equals("天地图影像")){
            uri = "http://t0.tianditu.com/img_w/wmts?service=wmts&request=GetTile&version=1.0.0&LAYER=img&tileMatrixSet=w&TileMatrix="+z+"&TileRow="+j+"&TileCol="+i+"&style=default&format=tiles&"+tk;
        }else if(type.equals("谷歌地图影像")) {
            uri = "http://mt1.google.cn/vt/lyrs=s&x="+i+"&y="+j+"&z="+z;
        }
        URL url = new URL(uri);
        URLConnection uc = url.openConnection();
        uc.setRequestProperty("User-Agent",USER_AGENT);
        InputStream inputStream = null;
        try {
            inputStream = uc.getInputStream();
            File dirFile = new File(filePath+"\\"+z);
            if (!dirFile.exists()) {
                dirFile.mkdir();
            }
            File dirFile2 = new File(filePath+"\\"+z+"\\"+i);
            if (!dirFile2.exists()) {
                dirFile2.mkdir();
            }
            FileOutputStream out = new FileOutputStream(filePath+"\\"+z+"\\"+i+"\\"+j+".png");
            int k = 0;
            while ((k = inputStream.read()) != -1) {
                out.write(k);
            }
            inputStream.close();
        } catch (IOException e) {
            //System.out.println("http://mt1.google.cn/vt/lyrs=s&x="+i+"&y="+j+"&z="+z);
            //System.out.println("x="+i+"&y="+j+"&z="+z);
            if(e.getMessage().contains("Server returned HTTP response code: 418")){
                jsonArray.getJSONObject(code).put("isSuspend", true);
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        textArea.appendText("此天地图tk今日调用次数已达到上限\n"+e.getMessage()+"\n");
                    }
                });
            }else if(e.getMessage().contains("http://mt1.google.cn/vt/lyrs=s")){

            }else{
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        textArea.appendText(e.getMessage()+"\n");
                    }
                });
            }
        }
    }

    public static void main(String[] args) throws IOException {
        //deg2num(120.440072,30.650569,12);
        //levet("70,54,133,2",2,"\\\\192.168.0.240\\WinServer F\\model\\tianditu");
        //Integer[] tileArr = deg2num(-180.00,180.00,1);
        //Integer[] tileArr2 = deg2num(180.00,0.00,1);
        //System.out.println(JSONObject.toJSONString(tileArr("118.78853439,28.59026991,118.84377771,28.55442852",17)));
    }

}
