package com.map.tile;

import com.map.tile.views.MainView;
import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class XxApplication extends AbstractJavaFxApplicationSupport {

    public static void main(String[] args) {
        launch(XxApplication.class, MainView.class, args);
        //SpringApplication.run(XxApplication.class, args);
    }

}
